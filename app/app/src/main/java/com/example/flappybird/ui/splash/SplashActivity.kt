package com.example.flappybird.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.flappybird.MainActivity
import com.example.flappybird.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    private val stepOfProgress = 2

    private val stepMultiplier = 100

    private val progressEndValue = 100

    private val progressDelay = 60L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        if (savedInstanceState == null) {
            progress(stepOfProgress)
        }
    }

    private fun progress(i: Int) {
        Handler().postDelayed({
            progressBar.background.level = i * stepMultiplier
            if (i < progressEndValue) {
                progress(i + stepOfProgress)
            } else {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }, progressDelay)
    }
}
