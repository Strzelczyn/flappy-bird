package com.example.flappybird.ui.game.play

import android.graphics.Canvas
import android.view.SurfaceHolder

class GameThread(private val surfaceHolder: SurfaceHolder, private val gameView: Game) : Thread() {

    private var running = false

    private var gameWork = true

    private val targetFPS = 30

    companion object {
        private var canvas: Canvas? = null
    }

    fun setRunning(isRunning: Boolean) {
        this.running = isRunning
    }

    fun setGameWorking(isWorking: Boolean) {
        this.gameWork = isWorking
    }

    fun getGameWorking(): Boolean {
        return gameWork
    }

    override fun run() {
        var startTime: Long
        var timeMillis: Long
        var waitTime: Long
        val targetTime = (1000 / targetFPS).toLong()
        while (running) {

            startTime = System.nanoTime()
            canvas = null

            try {
                canvas = this.surfaceHolder.lockCanvas()
                synchronized(surfaceHolder) {
                    if(gameWork == true) {
                        this.gameView.update()
                        this.gameView.draw(canvas!!)
                        this.gameView.collision()
                    }else {
                        this.gameView.draw(canvas!!)
                        this.gameView.drawLose(canvas!!)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            timeMillis = (System.nanoTime() - startTime) / 1000000
            waitTime = targetTime - timeMillis

            try {
                sleep(waitTime)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}