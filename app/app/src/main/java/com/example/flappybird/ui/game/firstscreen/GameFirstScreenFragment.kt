package com.example.flappybird.ui.game.firstscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.flappybird.R
import com.example.flappybird.databinding.FragmentGameFirstScreenBinding
import com.example.flappybird.ui.game.MySharedPreferences

class GameFirstScreenFragment : Fragment() {

    private var navControler: NavController? = null

    private lateinit var viewModel: GameFirstScreenViewModel

    private lateinit var binding: FragmentGameFirstScreenBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_game_first_screen, container, false)
        viewModel = ViewModelProviders.of(this).get(GameFirstScreenViewModel::class.java)
        viewModel.topScore.value = MySharedPreferences(context).restoreData()
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        registerObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navControler = Navigation.findNavController(view)
    }

    private fun registerObserver() {
        viewModel.gameStart.observe(viewLifecycleOwner, Observer<Boolean> {
            navControler!!.navigate(R.id.action_gameFirstScreenFragment_to_gameFragment)
        })
    }

}
