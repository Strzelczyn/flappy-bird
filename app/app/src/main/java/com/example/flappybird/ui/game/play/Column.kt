package com.example.flappybird.ui.game.play

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import kotlin.random.Random

class Column(var image: Bitmap) {

    var x: Int

    var y: Int

    val w: Int

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    private val xStep = screenWidth / 100

    private var flagForScore = true

    init {
        w = image.width
        x = screenWidth
        y = Random.nextInt(screenHeight / 2, screenHeight - 200)
    }

    fun draw(canvas: Canvas) {
        canvas.drawBitmap(image, x.toFloat(), y.toFloat(), null)
    }

    fun update(): Int {
        if (x < 0 - w) {
            x = screenWidth
            y = Random.nextInt(screenHeight / 2, screenHeight - screenHeight / 10)
            flagForScore = true
        } else {
            x -= (xStep)
        }
        if (flagForScore && x < (screenWidth / 2) - w) {
            flagForScore = false
            return 1
        } else {
            return 0
        }
    }

    fun restartState() {
        x = screenWidth
        y = Random.nextInt(screenHeight / 2, screenHeight - 200)
    }
}