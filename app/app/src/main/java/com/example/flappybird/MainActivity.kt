package com.example.flappybird

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.flappybird.ui.game.play.GameFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val currentFragment: Fragment?
        get() = nav_host_fragment.childFragmentManager.findFragmentById(R.id.nav_host_fragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_main)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val f = currentFragment
        if (f is GameFragment) {
            Navigation.findNavController(this, R.id.nav_host_fragment)
                .navigate(R.id.action_gameFirstScreenFragment_self)
        }
    }
}
