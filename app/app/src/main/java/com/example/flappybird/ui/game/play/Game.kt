package com.example.flappybird.ui.game.play

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.example.flappybird.R
import com.example.flappybird.ui.game.MySharedPreferences


class Game(context: Context, attributes: AttributeSet) : SurfaceView(context, attributes),
    SurfaceHolder.Callback {

    private val thread: GameThread

    private var column: Column? = null

    private var uperColumn: UperColumn? = null

    private var player: Player? = null

    private var background: Background? = null

    private var restart: Restart? = null

    private var touched: Boolean = false

    private var score = Score()

    private var lose = LoseText(context)

    init {
        holder.addCallback(this)
        thread = GameThread(holder, this)
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        column = Column(
            BitmapFactory.decodeResource(
                resources,
                R.drawable.column
            )
        )
        uperColumn = UperColumn(
            BitmapFactory.decodeResource(
                resources,
                R.drawable.column
            ), column!!.y
        )
        player = Player(
            BitmapFactory.decodeResource(
                resources,
                R.drawable.player
            )
        )
        background = Background(
            BitmapFactory.decodeResource(
                resources,
                R.drawable.main_background
            )
        )
        restart = Restart(
            BitmapFactory.decodeResource(
                resources,
                R.drawable.restart
            )
        )
        thread.setRunning(true)
        thread.start()
    }

    override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i1: Int, i2: Int) {}

    override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
        var retry = true
        while (retry) {
            try {
                thread.setRunning(false)
                thread.join()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            retry = false
        }
    }

    fun update() {
        score.value += column!!.update()
        uperColumn!!.update(column!!.y)
        if (touched) {
            player!!.updateTouch()
        } else {
            player!!.update()
        }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        background!!.draw(canvas)
        column!!.draw(canvas)
        uperColumn!!.draw(canvas)
        score.draw(canvas)
        if (touched && thread.getGameWorking()) {
            player!!.drawUP(canvas)
        } else {
            player!!.drawDown(canvas)
        }
    }

    fun drawLose(canvas: Canvas) {
        lose.draw(canvas)
        restart!!.draw(canvas)
    }

    fun collision() {
        if ((player!!.y >= player!!.screenHeight - player!!.h)
            || isCollisionDetected(
                player!!.image,
                player!!.x,
                player!!.y,
                column!!.image,
                column!!.x,
                column!!.y
            ) || isCollisionDetected(
                player!!.image,
                player!!.x,
                player!!.y,
                uperColumn!!.image,
                uperColumn!!.x,
                uperColumn!!.y
            )
        ) {
            stopGame()
        }
    }

    private fun stopGame() {
        MySharedPreferences(context).saveData(score.value)
        thread.setGameWorking(false)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val action = event.action
        if (thread.getGameWorking() == false) {
            val mx = event.x
            val my = event.y
            if (mx < restart!!.x + restart!!.w &&
                mx > restart!!.x &&
                my < restart!!.y + restart!!.h &&
                my > restart!!.y
            ) {
                restartGameState()
                thread.setGameWorking(true)
            }
        }

        when (action) {
            MotionEvent.ACTION_DOWN -> touched = true
            MotionEvent.ACTION_MOVE -> touched = true
            MotionEvent.ACTION_UP -> touched = false
            MotionEvent.ACTION_CANCEL -> touched = false
            MotionEvent.ACTION_OUTSIDE -> touched = false
        }
        return true
    }

    private fun restartGameState() {
        player!!.restartState()
        column!!.restartState()
        uperColumn!!.restartState(column!!.y)
        score.restartState()
    }

    fun isCollisionDetected(
        bitmap1: Bitmap, x1: Int, y1: Int,
        bitmap2: Bitmap, x2: Int, y2: Int
    ): Boolean {
        val width1 = bitmap1.width
        val height1 = bitmap1.height
        val width2 = bitmap2.width
        val height2 = bitmap2.height
        val bounds1 =
            Rect(x1, y1, x1 + width1, y1 + height1)
        val bounds2 =
            Rect(x2, y2, x2 + width2, y2 + height2)
        if (Rect.intersects(bounds1, bounds2)) {
            val collisionBounds: Rect = getCollisionBounds(bounds1, bounds2)
            for (i in collisionBounds.left until collisionBounds.right) {
                for (j in collisionBounds.top until collisionBounds.bottom) {
                    val bitmap1Pixel = bitmap1.getPixel(i - x1, j - y1)
                    val bitmap2Pixel = bitmap2.getPixel(i - x2, j - y2)
                    if (isFilled(bitmap1Pixel) && isFilled(bitmap2Pixel)) {
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun getCollisionBounds(
        rect1: Rect,
        rect2: Rect
    ): Rect {
        return Rect(
            Math.max(rect1.left, rect2.left),
            Math.max(rect1.top, rect2.top),
            Math.min(rect1.right, rect2.right),
            Math.min(rect1.bottom, rect2.bottom)
        )
    }

    private fun isFilled(pixel: Int): Boolean {
        return pixel != Color.TRANSPARENT
    }
}