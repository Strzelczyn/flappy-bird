package com.example.flappybird.ui.game.play

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.RectF

class Background(var image: Bitmap) {

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    fun draw(canvas: Canvas) {
        canvas.drawBitmap(
            image,
            null,
            RectF(0F, 0F, screenWidth.toFloat(), screenHeight.toFloat()),
            null
        )
    }
}