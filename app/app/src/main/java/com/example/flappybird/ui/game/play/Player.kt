package com.example.flappybird.ui.game.play

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix

class Player(val image: Bitmap) {

    var x: Int = 0

    var y: Int = 0

    val w: Int

    val h: Int

    private val dropDegressStep: Float = 1.0f

    private val maxDropDegressStep: Float = 30.0f

    private var upDegress: Float = -15.0f

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    private var dropDegress: Float = 0.0f

    private val onClikStep: Int = screenHeight / 30

    private val dropStep: Int = screenHeight / 100

    init {
        w = image.width
        h = image.height
        x = (screenWidth - w) / 2
        y = (screenHeight - h) / 2
    }

    fun drawUP(canvas: Canvas) {
        dropDegress = 0.0f
        canvas.drawBitmap(image.rotate(upDegress), x.toFloat(), y.toFloat(), null)
    }

    fun drawDown(canvas: Canvas) {
        if (dropDegress < maxDropDegressStep) {
            dropDegress += dropDegressStep
        }
        canvas.drawBitmap(image.rotate(dropDegress), x.toFloat(), y.toFloat(), null)
    }

    fun updateTouch() {
        if (y - onClikStep >= 0) {
            y -= onClikStep
        }
    }

    fun update() {
            y += dropStep
    }

    fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }
    fun restartState(){
        y = (screenHeight - h) / 2
        dropDegress = 0.0f
    }

}