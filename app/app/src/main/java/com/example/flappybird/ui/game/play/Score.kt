package com.example.flappybird.ui.game.play

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint

class Score {

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    private val x: Int = (screenWidth / 2) - (screenWidth / 90)

    private val y: Int = screenHeight / 4

    private val paint = Paint()

    var value: Int = 0

    init {
        paint.color = Color.WHITE
        paint.textSize = screenHeight / 20.0f
    }

    fun draw(canvas: Canvas) {
        canvas.drawText(value.toString(), x.toFloat(), y.toFloat(), paint)
    }

    fun restartState() {
        value = 0
    }
}