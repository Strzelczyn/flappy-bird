package com.example.flappybird.ui.game.play

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.example.flappybird.R

class LoseText(context: Context) {

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    private val x: Int = (screenWidth / 2) - (screenWidth / 5)

    private val y: Int = screenHeight / 3

    private val paint = Paint()

    private val text = context.getString(R.string.game_game_over)

    init {
        paint.color = Color.WHITE
        paint.textSize = screenHeight / 20.0f
    }

    fun draw(canvas: Canvas) {
        canvas.drawText(text, x.toFloat(), y.toFloat(), paint)
    }
}