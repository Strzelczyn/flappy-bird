package com.example.flappybird.ui.game.firstscreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GameFirstScreenViewModel : ViewModel() {

    val topScore = MutableLiveData<String>()

    val gameStart = MutableLiveData<Boolean>()

    fun startGame() {
        gameStart.value = true
    }

}
