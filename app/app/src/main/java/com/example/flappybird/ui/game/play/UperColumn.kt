package com.example.flappybird.ui.game.play

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix

class UperColumn(var image: Bitmap, yLowColumn: Int) {

    var x: Int

    var y: Int

    val w: Int

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    private val freeSpace = image.height + screenHeight / 4

    private val xStep = screenWidth / 100

    init {
        image = image.rotate(180.0f)
        w = image.width
        x = screenWidth
        y = yLowColumn - freeSpace
    }

    fun draw(canvas: Canvas) {
        canvas.drawBitmap(image, x.toFloat(), y.toFloat(), null)
    }

    fun update(yLowColumn: Int) {
        if (x < 0 - w) {
            x = screenWidth
            y = yLowColumn - freeSpace
        } else {
            x -= (xStep)
        }
    }

    fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    fun restartState(yLowColumn: Int) {
        x = screenWidth
        y = yLowColumn - freeSpace
    }
}