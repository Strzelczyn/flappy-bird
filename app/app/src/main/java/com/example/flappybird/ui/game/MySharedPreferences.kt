package com.example.flappybird.ui.game

import android.content.Context

class MySharedPreferences(context: Context?) {

    private val PREFERENCES_NAME = "myPreferences"

    private val preferences =
        context!!.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)

    fun saveData(score: Int) {
        if (score < restoreData().toInt()) {
            return
        }
        val editor = preferences.edit()
        editor.putString("topScore", score.toString())
        editor.apply()
    }

    fun restoreData(): String {
        return preferences!!.getString("topScore", "0").toString()
    }
}