package com.example.flappybird.ui.game.play

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas

class Restart(val image: Bitmap) {

    var x: Int = 0

    var y: Int = 0

    val w: Int

    val h: Int

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    init {
        w = image.width
        h = image.height
        x = (screenWidth - w) / 2
        y = (screenHeight - h) / 2
    }

    fun draw(canvas: Canvas) {
        canvas.drawBitmap(image, x.toFloat(), y.toFloat(), null)
    }
}
